locals {  rgs = {
          "alpha" = { "region" ="eastus"
                      "vnet" ="omega" }
          "bravo" = { "region" ="southindia"
                      "vnet" ="psi" }
          "charlie" = {"region" = "westus2"
                       "vnet" ="chi" }
                }}

resource "null_resource" "dummy_rgs" {
  triggers = {
               name= "???"     // alpha, bravo, charlie
               region= "???"   // eastus, southindia, westus2
}
}

resource "null_resource" "dummy_vnets" {
  triggers = {
               name= "???"        // use value of "vnet" above
               region= "???"      // use value of "region" above
}
}
