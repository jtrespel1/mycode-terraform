variable "container_name" {
  description = "Value of the name for the Docker container"
  # basic types include string, number and bool
  type        = string
  default     = "ExampleNginxContainer"
}

variable "internal_port" {
  description = "value of internal port number"
  type        = number
  default     = 80
}

variable "external_port" {
  description = "value of external port number"
  type        = number
  default     = 2224
}
