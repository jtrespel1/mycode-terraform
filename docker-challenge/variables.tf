variable "container_name" {
  description = "the container name. the name of the container. this container."
  type        = string
  default     = "AltaResearchWebService"
}

variable "internal_port" {
  description = "internal port number for container"
  type        = number
  default     = 9876
}

variable "external_port" {
  description = "the external port number for container"
  type        = number
  default     = 5432
}
